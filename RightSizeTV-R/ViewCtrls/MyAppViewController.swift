//
//  MyAppViewController.swift
//  RightSize_TV
//
//  Created by Saadhvi on 6/21/16.
//  Copyright © 2016 bby. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion

class MyAppViewController: UIViewController{
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCaptureStillImageOutput!
    var captureDevice : AVCaptureDevice?
    
    let motionManager:CMMotionManager = CMMotionManager()
    var strLevel: String!

    var distance:Float = 0.0
    var tilteAngle:Float = 0.0
    var height:Float = 0.0
    var deviceHeight:Float = 0.0
    
    var isViewed:Bool = false
    
    var lblStatus:UILabel!
    var lblInfo:UILabel!
    var btnRight:UIButton!
    var img_Wall:UIImage!
    
    @IBOutlet var vwCameraLayer: UIView!
    override func viewDidLoad() {
	
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
        captureSession.sessionPreset = AVCaptureSessionPreset1280x720
        self.captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if(captureDevice != nil){
            beginSession()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        strLevel = "Photo"
        doPlayingArea()
        doPutHeight()
        startMotionSensorCalculation(strLevel)
    }
    
    func doPlayingArea(){
        
        let lblVPoint = UILabel(frame: CGRectMake(self.view.frame.size.width / 2 - 2, self.view.frame.size.height / 2 - 25, 2, 50))
        lblVPoint.backgroundColor = UIColor.redColor()
        self.view!.addSubview(lblVPoint)
        
        let lblHPoint = UILabel(frame: CGRectMake(self.view.frame.size.width / 2 - 25, self.view.frame.size.height / 2 - 2, 50, 2))
        lblHPoint.backgroundColor = UIColor.redColor()
        self.view!.addSubview(lblHPoint)
        
        let vwLayerArea = UIView(frame: CGRectMake(0, self.view.frame.size.height - 120, self.view.frame.size.width, 120))
        vwLayerArea.backgroundColor = UIColor.clearColor()
        vwLayerArea.tag = 101
        self.view.addSubview(vwLayerArea)
        
        lblStatus = UILabel(frame: CGRectMake(0, 100, self.view.frame.size.width, 20))
        lblStatus.tag = 1
        lblStatus.backgroundColor = UIColor.whiteColor()
        lblStatus.textColor = UIColor.blackColor()
        lblStatus.font = UIFont.systemFontOfSize(10)
        lblStatus.textAlignment = .Center
        vwLayerArea.addSubview(lblStatus)
        
        lblInfo = UILabel(frame: CGRectMake(0, 80, self.view.frame.size.width, 20))
        lblInfo.tag = 2
        lblInfo.backgroundColor = UIColor.whiteColor()
        lblInfo.textColor = UIColor.blackColor()
        lblInfo.font = UIFont.systemFontOfSize(10)
        lblInfo.textAlignment = .Center
        vwLayerArea.addSubview(lblInfo)
        
        btnRight = UIButton(frame: CGRectMake(vwLayerArea.frame.size.width/2 -  17,CGRectGetMinY(lblInfo.frame) - 40, 40, 40))
        btnRight.setImage(UIImage(named:"capture"), forState: .Normal)
        btnRight.addTarget(self, action: #selector(actionBtnRight), forControlEvents: .TouchUpInside)
        btnRight.hidden = true
        vwLayerArea.addSubview(btnRight)
        
    }
    
    func doPutHeight(){
        
        let vwAlertBackLayer = UIView (frame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height));
        vwAlertBackLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        vwAlertBackLayer.tag = 111;
        self.view.addSubview(vwAlertBackLayer)
        
        let vwAlertLayer = UIView(frame:CGRectMake(vwAlertBackLayer.frame.size.width/2 - 125, vwAlertBackLayer.frame.size.height/2 - 100, 250, 180));
        vwAlertLayer.backgroundColor = UIColor(red: 224, green: 224, blue: 224, alpha: 1)
        vwAlertLayer.layer.cornerRadius = 10;
        vwAlertLayer.layer.masksToBounds = true;
        vwAlertBackLayer.addSubview(vwAlertLayer);
        
        let lblTitle = UILabel(frame:CGRectMake(0, 5, vwAlertLayer.frame.size.width, 15))
        lblTitle.font = UIFont.boldSystemFontOfSize(18)
        lblTitle.text = "RightSize TV"
        lblTitle.textAlignment = .Center
        lblTitle.textColor = UIColor.blackColor()
        vwAlertLayer.addSubview(lblTitle)
        
        let lblDescp = UILabel(frame:CGRectMake(0, CGRectGetMaxY(lblTitle.frame)+10, vwAlertLayer.frame.size.width, 15))
        lblDescp.font = UIFont.systemFontOfSize(13)
        lblDescp.text = "Enter your height in feet and inches"
        lblDescp.numberOfLines = 0
        lblDescp.textAlignment = .Center
        lblDescp.textColor = UIColor.blackColor()
        vwAlertLayer.addSubview(lblDescp)
        
        let txtFeet = UITextField(frame:CGRectMake(55, CGRectGetMaxY(lblDescp.frame) + 20, 50, 40));
        txtFeet.keyboardType = .NumberPad;
        txtFeet.font = UIFont.boldSystemFontOfSize(20)
        txtFeet.backgroundColor = UIColor.whiteColor();
        txtFeet.layer.borderWidth = 1;
        txtFeet.tag = 333;
        txtFeet.layer.borderColor = UIColor.blackColor().CGColor;
        vwAlertLayer.addSubview(txtFeet)
        
        let lblFeet = UILabel(frame:CGRectMake(CGRectGetMaxX(txtFeet.frame)+2, CGRectGetMaxY(lblDescp.frame) + 10, 15, 40))
        lblFeet.font = UIFont.systemFontOfSize(25)
        lblFeet.text = "\'"
        lblFeet.textColor = UIColor.blackColor()
        lblFeet.textAlignment = .Right;
        vwAlertLayer.addSubview(lblFeet)
        
        let txtInches = UITextField(frame:CGRectMake(CGRectGetMaxX(lblFeet.frame)+6, CGRectGetMaxY(lblDescp.frame) + 20, 50, 40));
        txtInches.keyboardType = .NumberPad;
        txtInches.font = UIFont.boldSystemFontOfSize(20)
        txtInches.backgroundColor = UIColor.whiteColor()
        txtInches.layer.borderWidth = 1;
        txtInches.tag = 444;
        txtInches.layer.borderColor = UIColor.blackColor().CGColor
        vwAlertLayer.addSubview(txtInches)
        
        let lblInches = UILabel(frame:CGRectMake(CGRectGetMaxX(txtInches.frame)+2, CGRectGetMaxY(lblDescp.frame) + 10, 15, 40))
        lblInches.font = UIFont.systemFontOfSize(25)
        lblInches.text = "\""
        lblInches.textColor = UIColor.blackColor()
        lblInches.textAlignment = .Right;
        vwAlertLayer.addSubview(lblInches);
        
        let vwHLine = UIView(frame:CGRectMake(0, CGRectGetMaxY(txtInches.frame) + 30, vwAlertLayer.frame.size.width, 2))
        vwHLine.backgroundColor = UIColor.darkGrayColor()
        vwAlertLayer.addSubview(vwHLine)
        
        let btnDone = UIButton(frame:CGRectMake(0, CGRectGetMaxY(vwHLine.frame) + 5, vwAlertLayer.frame.size.width, 35))
        btnDone.setTitleColor(UIColor(red: 70/255,green: 130/255,blue: 180/255,alpha: 1),forState:.Normal)
        btnDone.setTitle("Done",forState:.Normal)
        btnDone.addTarget(self,action:#selector(actionAlertDone),forControlEvents: .TouchUpInside)
        vwAlertLayer.addSubview(btnDone)
        
        vwAlertLayer.frame =  CGRectMake(vwAlertBackLayer.center.x, vwAlertBackLayer.center.y, 0, 0);
        UIView.animateWithDuration(1, animations: {
            vwAlertLayer.frame =  CGRectMake(vwAlertBackLayer.frame.size.width/2 - 125, vwAlertBackLayer.frame.size.height/2 - 100, 250, 180)
        })
    }
    
    func actionAlertDone() {
        
        let txtF = self.view.viewWithTag(333) as! UITextField
        let txtI = self.view.viewWithTag(444) as! UITextField
        if(ValidateNumber(String(format:"%@",txtI.text!)) && ValidateNumber(String(format:"%@",txtF.text!))) && !((String(format:"%@",txtF.text!) == "")){
            self.deviceHeight = Float(String(format:"%@.%@",txtF.text!,txtI.text!))!-0.5
            self.deviceHeight = (self.deviceHeight * Constants.feetToCm);
            self.view.endEditing(true)
            
            let view = self.view.viewWithTag(111)
            view?.removeFromSuperview()
        }
        else{
            txtF.text = ""
            txtI.text = ""
            txtF.layer.borderColor = UIColor.redColor().CGColor
            txtI.layer.borderColor = UIColor.redColor().CGColor
        }
    }
    
    func beginSession() {
        
        self.stillImageOutput = AVCaptureStillImageOutput()
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            self.captureSession.addOutput(self.stillImageOutput)
            
            do{
                self.captureSession.addInput(try AVCaptureDeviceInput(device: self.captureDevice))
            }
            catch{
                print("Capture session Error")
            }
            let previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            previewLayer?.frame = self.vwCameraLayer.layer.bounds
            previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            dispatch_async(dispatch_get_main_queue(), {
                self.vwCameraLayer.layer.addSublayer(previewLayer)
                self.captureSession.startRunning()
            });
        });
    }
    
    func  startMotionSensorCalculation(strL:String){
        
        self.motionManager.deviceMotionUpdateInterval = 0.2;
        self.motionManager.startDeviceMotionUpdatesUsingReferenceFrame(.XArbitraryZVertical, toQueue:NSOperationQueue.mainQueue(), withHandler: { (dm, error) in
            let x:Double = (dm?.gravity.x)!
            let y:Double = (dm?.gravity.y)!
            let z:Double = (dm?.gravity.z)!
            
            let r:Double = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2))
            if(r == 0.0){
                self.tilteAngle = Float(r)
            }
            else{
                self.tilteAngle = acosf(Float(z)/Float(r))
            }
            let rotationAngleDegree = self.tilteAngle * 180.0 / Float(M_PI)
            
            if(self.strLevel == "Bottom"){
                self.btnRight.hidden = false
                self.distance = self.deviceHeight * tanf(self.tilteAngle);
                if(self.distance < 0.0){
                 self.distance = -(self.distance)
                }
                self.lblInfo.text = Constants.strLblInfoDistance
                self.lblStatus.text = String(format:"Distance to the wall is %.2f feet at a %.2f° angle ",self.deviceHeight,(self.distance * Constants.cmTofeet),rotationAngleDegree)
            }
            else if(self.strLevel == "Top"){
                self.btnRight.hidden = false
                self.height = (self.distance / tanf(-(self.tilteAngle))) - self.deviceHeight;
                if(self.height < 0.0){
                    self.height = -(self.height)
                }
                self.lblInfo.text = Constants.strLblInfoHeight
                self.lblStatus.text = String(format:"Height of the wall is %.2f feet at a %.2f° angle",(self.height * Constants.cmTofeet),rotationAngleDegree,self.deviceHeight)
            }
            else if(self.strLevel == "Photo"){
                self.lblStatus.text = String(format:"Angle %.1f°",rotationAngleDegree)
                if((rotationAngleDegree > 89.5) && (rotationAngleDegree < 90.5)){
                    AudioServicesPlaySystemSound(Constants.systemid)
                    if(!self.isViewed){
                        self.btnRight.hidden = false
                        self.isViewed = true
                    }
                }
                else{
                    if(self.isViewed){
                        self.btnRight.hidden = true
                        self.isViewed = false
                    }
                }
            }
        })
    }
    
    func actionBtnRight() {
        
        if(strLevel == "Bottom"){
            startMotionSensorCalculation(strLevel)
            strLevel = "Top"
        }
        else if(strLevel == "Top"){
            let TVVC : TvSizeViewController = self.storyboard!.instantiateViewControllerWithIdentifier("TvSizeViewController") as! TvSizeViewController
            TVVC.wallImage = img_Wall
            if(self.distance < 0.0){
                TVVC.tvdiagonal = -(self.distance)
            }
            else{
                TVVC.tvdiagonal = self.distance
            }
            if(self.height < 0.0){
                TVVC.imageHeight = CGFloat(-(self.height))
            }
            else{
                TVVC.imageHeight = CGFloat(self.height)
            }
            self.presentViewController(TVVC, animated: true, completion: nil)
        }
        else if(strLevel == "Photo"){
            
            self.motionManager.stopDeviceMotionUpdates()
            strLevel = "Bottom"
            imageCapture()
        }
        
    }
    
    func actionBtnChangeHeight(){
        
       // doPutHeight()
    }

    func imageCapture(){
//        stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
//        captureSession.addOutput(stillImageOutput)
        
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo){
            
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {
                (sampleBuffer, error) in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                let dataProvider = CGDataProviderCreateWithCFData(imageData)
                let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
               
                self.img_Wall = image
                self.startMotionSensorCalculation(self.strLevel)
            })
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        super.viewDidDisappear(animated)
        
            if let vwLArea:UIView = self.view.viewWithTag(101){
                vwLArea.removeFromSuperview()
            }
            self.motionManager.stopDeviceMotionUpdates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
