//
//  TvSizeViewController.swift
//  RightSize_TV
//
//  Created by Saadhvi on 7/1/16.
//  Copyright © 2016 bby. All rights reserved.
//

import UIKit


class TvSizeViewController: UIViewController {

    var lblTvBox: UILabel! = nil
    var lblBoaderLineFloor: UIImageView! = nil
    var lblBoaderLineRoof: UIImageView! = nil
    var btnBack: UIButton! = nil
    var lblSelectedTV: UILabel! = nil
    var activityIndicator: UIActivityIndicatorView! = nil
    var floorLine: UIPanGestureRecognizer! = nil
    var roofLine: UIPanGestureRecognizer! = nil
    var panTVImage: UIPanGestureRecognizer! = nil
    var pinchGesture: UIPinchGestureRecognizer! = nil
    var panGesture: UIPanGestureRecognizer! = nil
    
    var requestReply: String = ""
    var json: NSMutableDictionary = [:]
    var aryTvProducts: NSMutableArray = []
    
    let pageNo:Int = 0
    var totalPages:Int = 0
    var wallWidth:CGFloat=0.0
    var imageHeightPx: CGFloat = 0.0
    var imagewidPx: CGFloat = 0.0
    var tvHeight: CGFloat = 0.0
    var tvWidth:CGFloat = 0.0
    var tvdiagonal:Float=0.0
    var ratio:Float = 1.0
    
    @IBOutlet var vwStudyLayer:UIView!
    @IBOutlet var btnGotit:UIButton!
    @IBOutlet var btnOk:UIButton!
    @IBOutlet var imgIns:UIImageView!
    @IBOutlet var collTblTVList:UICollectionView!
    @IBOutlet var imgWallView: UIImageView!
    
    var wallImage: UIImage!
    var imageHeight: CGFloat!
    var distance: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        tvHeight = CGFloat(Constants.rTvHeight);
        tvWidth = CGFloat(Constants.rTvWidth);
        //tvWidth = (tvHeight * 16) / 9;
        ratio = 1.0;
        self.imgWallView.image = self.wallImage
        
        lblTvBox = UILabel()
        lblTvBox.backgroundColor = UIColor(red: 128/255, green:128/255, blue:128/255, alpha:0.6)
        lblTvBox.layer.borderWidth = 2
        lblTvBox.layer.borderColor = UIColor(red: 70/255, green:130/255, blue:180/255, alpha:1).CGColor
        lblTvBox.textColor = UIColor.whiteColor()
        lblTvBox.textAlignment = .Center;
        lblTvBox.userInteractionEnabled = true;
        lblTvBox.font = UIFont.systemFontOfSize(10)
        lblTvBox.sizeToFit()
        self.view.addSubview(lblTvBox)
        
        // btnOk.frame = CGRectMake(self.view.frame.size.width/2-35, self.view.frame.size.height-50, 70, 70);
        lblBoaderLineFloor = UIImageView(frame:CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 30))
        lblBoaderLineFloor.image = UIImage(named:"imgGreenLine.png")
        lblBoaderLineFloor.contentMode = .ScaleAspectFill
        lblBoaderLineFloor.userInteractionEnabled = true
        self.view.addSubview(lblBoaderLineFloor)
        
        lblBoaderLineRoof = UIImageView(frame:CGRectMake(0, 250, self.view.frame.size.width, 30))
        lblBoaderLineRoof.image = UIImage(named:"imgGreenLine.png")
        lblBoaderLineRoof.contentMode = .ScaleAspectFill
        lblBoaderLineRoof.userInteractionEnabled = true
        self.view.addSubview(lblBoaderLineRoof)
        
        btnOk.layer.cornerRadius = 10;
        self.view.addSubview(btnOk)
        
        collTblTVList.hidden = true
        self.view.addSubview(collTblTVList)
        
        btnBack = UIButton(frame:CGRectMake(5, 60, 35, 35))
        btnBack.addTarget(self, action:#selector(goToBack), forControlEvents:.TouchUpInside)
        btnBack.setImage(UIImage(named:"imgLeftArrow"), forState: .Normal)
        self.view.addSubview(btnBack)
        
        lblSelectedTV = UILabel()
        lblSelectedTV.backgroundColor = UIColor.blackColor()
        lblSelectedTV.textColor = UIColor.whiteColor()
        lblSelectedTV.textAlignment = .Center
        lblSelectedTV.font = UIFont.systemFontOfSize(12)
        lblSelectedTV.numberOfLines = 0;
        lblSelectedTV.userInteractionEnabled = true;
        lblSelectedTV.hidden = true;
        self.view.addSubview(lblSelectedTV)
        
        btnGotit.frame = CGRectMake(vwStudyLayer.frame.size.width/2 - 125, vwStudyLayer.frame.size.height - 60, 250, 50);
        btnGotit.layer.cornerRadius = 10;
        vwStudyLayer.addSubview(btnGotit)
        self.view.addSubview(vwStudyLayer)
        
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        aryTvProducts = NSMutableArray()
        if Reachbility.isConnectedToNetwork() == false{
            let alert = UIAlertView(title: "RightSize TV", message: "Please ensure your iPhone/iPad is connected to Internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        let result: CGSize = UIScreen.mainScreen().bounds.size
        if (UIDevice.currentDevice().userInterfaceIdiom == .Phone) {
            
            if (result.height == 736) {
                // 5.5 inch display - iPhone 6 Plus
                ratio = 0.8;
            }
        }
        
        var floorBWRoof:CGFloat = CGRectGetMidY(lblBoaderLineRoof.frame) - CGRectGetMidY(lblBoaderLineFloor.frame);
        if(floorBWRoof < 0){
            floorBWRoof = -(floorBWRoof)
        }
        calculateTVSize(Float(floorBWRoof) * ratio)
        
        panTVImage = UIPanGestureRecognizer(target: self, action:#selector(TvSizeViewController.handlePanTVImage(_:)))
        lblSelectedTV.addGestureRecognizer(panTVImage)
        //move the border Line Gesture Recognizer
        floorLine = UIPanGestureRecognizer(target: self, action:#selector(TvSizeViewController.handleFloorLine(_:)))
        lblBoaderLineFloor.addGestureRecognizer(floorLine)
        
        roofLine = UIPanGestureRecognizer(target:self, action: #selector(TvSizeViewController.handleRoofLine(_:)))
        lblBoaderLineRoof.addGestureRecognizer(roofLine)
        
        //move the Tv Box Gesture Recognizer
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(TvSizeViewController.handlePinch(_:)))
        lblTvBox.addGestureRecognizer(pinchGesture)
        //crop the Tv Box Gesture Recognizer
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(TvSizeViewController.handlePan(_:)))
        lblTvBox.addGestureRecognizer(panGesture)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func handlePanTVImage(recognizer: UIPanGestureRecognizer) {
        // drag the Tv image box
        let translation: CGPoint = recognizer.translationInView(self.view!)
        recognizer.view!.center = CGPointMake(recognizer.view!.center.x + translation.x, recognizer.view!.center.y + translation.y)
        recognizer.setTranslation(CGPointMake(0, 0), inView: self.view!)
     }

    func handlePan(recognizer: UIPanGestureRecognizer) {
        
        let translation: CGPoint = recognizer.translationInView(self.view!)
        recognizer.view!.center = CGPointMake(recognizer.view!.center.x + translation.x, recognizer.view!.center.y + translation.y)
        recognizer.setTranslation(CGPointMake(0, 0), inView: self.view!)
        if recognizer.view!.frame.origin.x < 0.0 || recognizer.view!.frame.origin.x + recognizer.view!.frame.size.width > self.imgWallView.frame.size.width || recognizer.view!.frame.origin.y < 0.0 || recognizer.view!.frame.origin.y + recognizer.view!.frame.size.height > self.imgWallView.frame.size.height {
            lblTvBox.layer.borderColor = UIColor.redColor().CGColor
        }
        else {
            lblTvBox.layer.borderColor = UIColor(red: 70/255, green:130/255, blue:180/255, alpha:1).CGColor
        }
        tvdiagonal = sqrtf(pow(Float(tvWidth),2) + pow(Float(tvHeight),2))
        lblTvBox.text = String(format: "W = %.1f-H = %.1f", Float(tvWidth) * Constants.cmToInch, Float(tvHeight) * Constants.cmToInch)
    }
    
    func handleFloorLine(recognizer: UIPanGestureRecognizer) {
        //drag the border Line (Floor)
        let translation: CGPoint = recognizer.translationInView(self.view!)
        recognizer.view!.center = CGPointMake(recognizer.view!.center.x, recognizer.view!.center.y + translation.y)
        recognizer.setTranslation(CGPointMake(0, 0), inView: self.view!)
        var floorBWRoof: CGFloat = CGRectGetMidY(recognizer.view!.frame) - CGRectGetMidY(lblBoaderLineRoof.frame)
        floorBWRoof = (floorBWRoof < 0) ? -(floorBWRoof) : floorBWRoof
        calculateTVSize(Float(floorBWRoof) * ratio)
    }
    
    func handleRoofLine(recognizer: UIPanGestureRecognizer) {
        //drag the border Line (Roof)
        let translation: CGPoint = recognizer.translationInView(self.view!)
        recognizer.view!.center = CGPointMake(recognizer.view!.center.x, recognizer.view!.center.y + translation.y)
        recognizer.setTranslation(CGPointMake(0, 0), inView: self.view!)
        var floorBWRoof: CGFloat = CGRectGetMidY(recognizer.view!.frame) - CGRectGetMidY(lblBoaderLineFloor.frame)
        floorBWRoof = (floorBWRoof < 0) ? -(floorBWRoof) : floorBWRoof
        calculateTVSize(Float(floorBWRoof) * ratio)
    }
    
    func handlePinch(recognizer: UIPinchGestureRecognizer) {
        
        var theSlope: CGFloat
        if recognizer.numberOfTouches() > 1 {
            
            let theView: UIView = recognizer.view!
            let locationOne: CGPoint = recognizer.locationOfTouch(0, inView: theView)
            let locationTwo: CGPoint = recognizer.locationOfTouch(1, inView: theView)
            if locationOne.x == locationTwo.x {
                theSlope = 1000.0
            }
            else if locationOne.y == locationTwo.y {
                theSlope = 0.0
            }
            else {
                theSlope = (locationTwo.y - locationOne.y) / (locationTwo.x - locationOne.x)
            }
            let abSlope: Double = abs(Double(theSlope))
            if abSlope < 0.5 {
                // Horizontal pinch - scale in the X
                lblTvBox.transform = CGAffineTransformScale(lblTvBox.transform, recognizer.scale, 1.0)
            }
            else {
                // Vertical pinch - scale in the Y
                lblTvBox.transform = CGAffineTransformScale(lblTvBox.transform, 1.0, recognizer.scale)
            }
            recognizer.scale = 1.0
        }
        if recognizer.view!.frame.origin.x < 0.0 || recognizer.view!.frame.origin.x + recognizer.view!.frame.size.width > self.imgWallView.frame.size.width || recognizer.view!.frame.origin.y < 0.0 || recognizer.view!.frame.origin.y + recognizer.view!.frame.size.height > self.imgWallView.frame.size.height {
            lblTvBox.layer.borderColor = UIColor.redColor().CGColor
        }
        else {
            lblTvBox.layer.borderColor = UIColor(red: 70/255, green:130/255, blue:180/255, alpha:1).CGColor
        }
        
        tvHeight = (tvHeight * recognizer.view!.frame.size.height) / imageHeightPx
        tvWidth = (tvWidth * recognizer.view!.frame.size.width) / imagewidPx
        tvdiagonal = sqrtf(pow(Float(tvWidth),2) + pow(Float(tvHeight),2))
        imageHeightPx = recognizer.view!.frame.size.height
        imagewidPx = recognizer.view!.frame.size.width
        lblTvBox.text = String(format: "W = %.1f H = %.1f", Float(tvWidth) * Constants.cmToInch, Float(tvHeight) * Constants.cmToInch)
    }
    
    func calculateTVSize(PixelSize: Float) {
        //pixel calculation.
        imageHeightPx = (CGFloat(PixelSize) * tvHeight) / imageHeight
        wallWidth = (self.view.frame.size.width * imageHeight) / CGFloat(PixelSize)
        imagewidPx = (self.view.frame.size.width * tvWidth) / wallWidth
        //imagewidPx = (tvWidth * imageHeightPx) / tvHeight;
        tvdiagonal = sqrtf(pow(Float(tvWidth),2) + pow(Float(tvHeight),2))
        lblTvBox.text = String(format: "W = %.1f H = %.1f", Float(tvWidth) * Constants.cmToInch, Float(tvHeight) * Constants.cmToInch)
        lblTvBox.frame = CGRectMake(self.view.frame.size.width / 2 - imagewidPx / 2, self.view.frame.size.height / 2 - imageHeightPx / 2, imagewidPx, imageHeightPx)
    }
    
    @IBAction func actionTvList(sender: UIButton) {
        
        UIView.transitionWithView(collTblTVList, duration: 0.4, options: .TransitionFlipFromTop, animations: {() -> Void in
            self.collTblTVList.hidden = false
            }, completion: { _ in })
        aryTvProducts.removeAllObjects()
        lblTvBox.removeGestureRecognizer(panGesture)
        lblBoaderLineFloor.removeGestureRecognizer(floorLine)
        lblBoaderLineRoof.removeGestureRecognizer(roofLine)
        lblTvBox.removeGestureRecognizer(pinchGesture)
        lblBoaderLineFloor.image = UIImage(named: "imgRedLine.png")
        lblBoaderLineRoof.image = UIImage(named: "imgRedLine.png")
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.frame = CGRectMake(0, 0, 30, 30)
        activityIndicator.center = collTblTVList.center
        self.view!.addSubview(activityIndicator)
        self.sendHTTPGetTVWSize(tvWidth, tvHSize: tvHeight)
        btnOk.removeFromSuperview()
    }
    
    @IBAction func actionGotIT(sender: UIButton) {
        vwStudyLayer.removeFromSuperview()
    }
    
    func sendHTTPGetTVWSize(tvWSize: CGFloat, tvHSize: CGFloat) {
        //call api for the List of TV products
        let request: NSMutableURLRequest = NSMutableURLRequest()
        let strURL = String(format:"%@(categoryPath.id=%@&width<=%.1f&heightWithoutStandIn>0)?show=sku,name,salePrice,shortDescription,mediumImage,customerReviewCount,width,heightWithoutStandIn,image&sort=width.desc&pageSize=6&apiKey=%@&format=json",Constants.BASEURL,Constants.bbyCategory,Float(tvWSize)*Constants.cmToInch,Constants.bestBuyApiKey)
    
        request.URL = NSURL(string: strURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLFragmentAllowedCharacterSet())!)!
        request.HTTPMethod = "GET"
        let sessionConf: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session: NSURLSession = NSURLSession(configuration:sessionConf)
        session.dataTaskWithRequest(request, completionHandler: {(data, response, error) -> Void in
        //self.requestReply = String(data, NSUTF8StringEncoding)
        self.ConvertToJson(data!)
        }).resume()
        
    }
    
    func ConvertToJson(dtJson: NSData) {
        //pick the list of recommended TV products.
        //let jsonData: NSData = strJson.dataUsingEncoding(NSUTF8StringEncoding)!
        do{
            json = try NSJSONSerialization.JSONObjectWithData(dtJson, options: .AllowFragments) as! NSMutableDictionary
            totalPages = json.objectForKey("totalPages") as! Int
            aryTvProducts.addObjectsFromArray(json.objectForKey("products") as! Array)
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                self.collTblTVList.reloadData()
            })
        }catch{
            print(error)
        }
    }
    
    func goToBack() {
        //self.navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Collection view dataSource and delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryTvProducts.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let TvProductCell: UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("TVCollectionCell", forIndexPath: indexPath)
        let tvProductImage: UIImageView = (TvProductCell.viewWithTag(1) as! UIImageView)
        let tvProductTitle: UILabel = (TvProductCell.viewWithTag(2) as! UILabel)
        let tvProductPrice: UILabel = (TvProductCell.viewWithTag(3) as! UILabel)
        let dicTv: NSDictionary = aryTvProducts[indexPath.row] as! NSDictionary
    
        tvProductTitle.text = String(format: "%@", dicTv.valueForKey("name") as! String)
        tvProductPrice.text = String(format: "$ %.2lf", dicTv.valueForKey("salePrice") as! Double)
        if(dicTv.valueForKey("mediumImage") != nil){
            tvProductImage.setImageWithURL(NSURL(string: dicTv.valueForKey("mediumImage") as! String)!, usingActivityIndicatorStyle: .Gray)
        }else if(dicTv.valueForKey("image") != nil){
            tvProductImage.setImageWithURL(NSURL(string: dicTv.valueForKey("image") as! String)!, usingActivityIndicatorStyle: .Gray)
        }

        activityIndicator.stopAnimating()
        return TvProductCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let dicTv:NSDictionary = aryTvProducts[indexPath.row] as! NSDictionary
        
        var strWidth: String = String(format: "%@", (dicTv.valueForKey("width") as! String))
        strWidth = strWidth.stringByReplacingOccurrencesOfString("\"", withString: "")
        let strHeight: String = String(format: "%.1f", (dicTv.valueForKey("heightWithoutStandIn") as! Float))
        let sltTVWidth: Float = Float(strWidth)!
        let sltTVHeight: Float = Float(strHeight)!
        tvWidth = CGFloat(sltTVWidth * Constants.inchToCm)
        tvHeight = CGFloat(sltTVHeight * Constants.inchToCm)
        var floorBWRoof:CGFloat = CGRectGetMidY(lblBoaderLineRoof.frame) - CGRectGetMidY(lblBoaderLineFloor.frame);
        if(floorBWRoof < 0){
            floorBWRoof = -(floorBWRoof)
        }
        imageHeightPx = (floorBWRoof * tvHeight) / self.imageHeight
        wallWidth = (self.view.frame.size.width * self.imageHeight) / floorBWRoof
        //imagewidPx = (tvWidth * imageHeightPx) / tvHeight;
        
        imagewidPx = (self.view.frame.size.width * tvWidth) / wallWidth
        tvdiagonal = sqrtf(pow(Float(tvWidth),2) + pow(Float(tvHeight),2))

        lblSelectedTV.frame.size = CGSizeMake(imagewidPx, imageHeightPx)
        lblSelectedTV.center = lblTvBox.center
        lblSelectedTV.hidden = false
        lblSelectedTV.text = String(format: "%@ Width - %@ Height - %@ price - $ %.2lf SKU - %ld",dicTv.valueForKey("name") as! String,strWidth,strHeight,dicTv.valueForKey("salePrice") as! Double,(dicTv.valueForKey("sku") as! NSNumber).longLongValue)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
