//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation
import UIKit

extension String {
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
}

struct Constants {
    
    static let rTvHeight:Float       = 45.974  //32.258
    static let rTvWidth:Float        = 81.788 //57.658
    static let cmToInch:Float        = 0.393701
    static let inchToCm:Float        = 2.54
    static let feetToCm:Float        = 30.48
    static let cmTofeet:Float        = 0.0328084
    static let kUpdateInterval:Double = (10.0 / 60.0)
    static let bestBuyApiKey:String   = "3jgkqpbxyb4b4jce4an5yzdw"
    static let BASEURL:String         = "https://api.bestbuy.com/v1/products"
    static let bbyCategory:String     = "abcat0101001"
    static let systemid:UInt32           = 4095
       
    static let strLblInfoDistance:String = "Align center marker where the wall meets the floor"
    static let strLblInfoHeight:String = "Align center marker where the wall meets the ceiling"
}

public func ValidateNumber(number:String) -> Bool{
    
    let NumRegEx:String = "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$"
    let regExPredicate:NSPredicate = NSPredicate(format:"SELF MATCHES %@", NumRegEx)
    let myStringMatches:Bool = regExPredicate.evaluateWithObject(number)
    return myStringMatches
}


