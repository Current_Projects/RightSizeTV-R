//
//  ViewController.swift
//  RightSizeTV-R
//
//  Created by Saadhvi on 7/22/16.
//  Copyright © 2016 bby. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var btnStarted: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        btnStarted.layer.cornerRadius = 10
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

